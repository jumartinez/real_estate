from django import forms


class SearchNeighborhoodForm(forms.Form):

    email = forms.EmailField()
    neighborhood = forms.CharField(max_length=40)

