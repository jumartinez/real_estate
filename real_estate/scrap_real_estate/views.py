from celery import chain, chord, group
from django.contrib import messages
from django.urls import reverse_lazy
from django.views.generic import FormView
from .forms import SearchNeighborhoodForm
from .utils.scraping import scraping_casas_minuta, scrapin_bozzimbett
from .tasks import celery_scraping_real_estate, celery_send_mail, celery_scrapin_bozzimbett
from django.conf import settings
# Create your views here.


class SearchNeighborhoodView(FormView):
    extra_context = {"title": "Search a neighborhood"}
    template_name = "search_neighborhood.html"
    form_class = SearchNeighborhoodForm
    success_url = reverse_lazy('scrap')

    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS, "Your message was sended successful")
        neighborhood = self.request.POST['neighborhood']
        (group([celery_scraping_real_estate.s(neighborhood), celery_scrapin_bozzimbett.s(neighborhood)])|\
            (celery_send_mail.s("Real-Estate", settings.EMAIL_HOST_USER, self.request.POST.get('email'))))()
        return super(SearchNeighborhoodView, self).form_valid(form)
