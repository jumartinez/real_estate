from real_estate.celery import app
from .utils.scraping import scraping_casas_minuta, scrapin_bozzimbett
from .utils.send_mail import send_mail


@app.task
def celery_scraping_real_estate(neighborhood):
    return scraping_casas_minuta(neighborhood)


@app.task
def celery_scrapin_bozzimbett(neighborhood):
    return scrapin_bozzimbett(neighborhood)


@app.task
def celery_send_mail(data, subject, from_email, to):
    return send_mail(data, subject, from_email, to)
