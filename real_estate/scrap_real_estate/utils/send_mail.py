from django.core.mail import EmailMessage, send_mail as send, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags


def send_mail(data, subject, from_email, to):
    first_page_ads = data[0][0]["ads"]
    second_page_ads = data[1][0]["ads"]
    first_page_links = data[0][0]["links"]
    second_page_links = data[1][0]["links"]
    ads = first_page_ads + second_page_ads
    links = first_page_links + second_page_links
    html_msg = render_to_string("send_mail.html", {"results": list(zip(ads, links))})
    plain_text = strip_tags(html_msg)
    send(subject, plain_text, from_email, [to], html_message=html_msg)


