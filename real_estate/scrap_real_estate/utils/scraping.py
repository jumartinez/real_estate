from selenium import webdriver
import time


def scraping_casas_minuta(neighborhood):
    driver = webdriver.Chrome()
    driver.implicitly_wait(1)
    driver.get(f"https://casas.mitula.com.co/casas/locales-cartagena-{neighborhood}")
    div_r = driver.find_elements_by_class_name('lis_ting_Ad')
    list_data = []
    list_link = []
    result = []
    for div in div_r:
        link = div.find_element_by_tag_name('a').get_attribute('onmousedown').split('"')[1]
        data = div.text
        data = data.replace("\n", "")
        data = data.replace("\t", "")
        list_data.append(data.strip())
        list_link.append(link)
        result.append({"ads": list_data, "links": list_link})
    return result


def scrapin_bozzimbett(neighborhood):
    driver = webdriver.Chrome()
    driver.implicitly_wait(1)
    driver.get("http://bozzimbett.com/")
    driver.find_element_by_xpath("//select[@name='bus_localizacion']/option[text()='Cartagena']").click()
    driver.find_element_by_xpath('//*[@id="mini-panel-buscador_home"]/div[2]/div[1]/div/div/div/div[3]/div/input')\
        .click()
    select = driver.find_element_by_name("facets")
    neighborhood_list = [x.text.lower() for x in select.find_elements_by_tag_name("option")]
    res = [s for s in neighborhood_list if neighborhood in s]
    try:
        res = f' {" ".join(map(lambda x:x.capitalize(),res.pop()[1:].split(" ")))}'
        driver.find_element_by_xpath(f"//select[@name='facets']/option[text()='{res}']").click()
        div_r = driver.find_elements_by_class_name('views-field')
        list_data = []
        list_link = []
        result = []
        for data in div_r:
            link = data.find_element_by_tag_name('a').get_attribute('href')
            data = data.text
            data = data.replace("\n", " ")
            data = data.replace("\t", " ")
            data = data.strip()
            list_data.append(data)
            list_link.append(link)
            result.append({"ads": list_data, "links": list_link})
        return result
    except IndexError as ie:
        return [{"ads": [], "links": []}]

