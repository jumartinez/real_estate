from django.urls import path
from .views import SearchNeighborhoodView

urlpatterns = [
    path('', SearchNeighborhoodView.as_view(), name="scrap"),
]
