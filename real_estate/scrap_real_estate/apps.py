from django.apps import AppConfig


class ScrapRealEstateConfig(AppConfig):
    name = 'scrap_real_estate'
