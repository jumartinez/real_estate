from django.contrib.auth import get_user_model
from django.db import models

# Create your models here.
User = get_user_model()

class Anime(models.Model):

    anime_id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    genre = models.CharField(max_length=255)
    anime_type = models.CharField(max_length=255)
    episodes = models.IntegerField()
    anime_rating = models.FloatField()
    members = models.BigIntegerField()


class AnimeRating(models.Model):

    user_id = models.IntegerField()
    anime_id = models.ForeignKey(Anime, on_delete=models.CASCADE)
    rating = models.FloatField()
