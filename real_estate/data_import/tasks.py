from django.contrib.auth import get_user_model

from real_estate.celery import app
import pandas as pd
from data_import.models import Anime, AnimeRating


@app.task
def import_anime_csv(file_path):
    data = pd.read_csv(file_path).fillna(value=0)
    for index, row in data.iterrows():
        if len(data.columns.values) > 3:
            if not (int(row["Unnamed: 7"]) | int(row['Unnamed: 8']) | int(row['Unnamed: 9'])):
                try:
                    Anime.objects.update_or_create(anime_id=row['anime_id'], name=row['name'], genre=row['genre'],
                                                   anime_type=row['type'], episodes=row['episodes'],
                                                   anime_rating=row['rating'], members=row['members'])
                except ValueError as ex:
                    continue
        if len(data.columns.values) <= 4:
            if not row.get("Unnamed: 4", False):
                if Anime.objects.filter(anime_id=row['anime_id']).exists():
                    try:
                        anime = Anime.objects.get(anime_id=row['anime_id'])
                        AnimeRating.objects.update_or_create(rating=row['rating'], anime_id=anime,
                                                             user_id=row['user_id'])
                    except ValueError as ex:
                        continue
