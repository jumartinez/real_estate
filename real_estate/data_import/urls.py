from django.urls import path
from .views import ImportAnimeFileView

urlpatterns = [
    path('', ImportAnimeFileView.as_view(), name="read_csv"),
]
