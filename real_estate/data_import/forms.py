from django import forms


class UploadCSVFileForm(forms.Form):

    upload_file = forms.FileField(allow_empty_file=False)