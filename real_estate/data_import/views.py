# Create your views here.
from django.contrib import messages
from django.urls import reverse_lazy
from django.views.generic import FormView
from .forms import UploadCSVFileForm
from django.conf import settings
from data_import.tasks import import_anime_csv
import pandas as pd

class ImportAnimeFileView(FormView):
    extra_context = {'title': "Upload file"}
    template_name = "import_csv.html"
    form_class = UploadCSVFileForm
    success_url = reverse_lazy('read_csv')

    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS, "Your file was uploaded successful")
        csv_file = self.request.FILES['upload_file']
        if not csv_file.name.endswith('.csv'):
            messages.error(self.request, 'File is not CSV type')
        path_file = f'{settings.BASE_DIR}/{csv_file.name}'
        with open(path_file, 'wb') as file:
            file.write(csv_file.read())
        import_anime_csv.delay(path_file)
        return super(ImportAnimeFileView, self).form_valid(form)



